/*
 * Classe: TotaisMsgBean
 * 
 * Data: 13/12/2004
 * 
 */
package br.gov.serpro.ouvidoria.bean;

/**
 * Objetivo: Auxiliar na exibição das informações das mensagens pendentes
 * 
 * @author SERPRO
 * @version $Revision: 1.1.2.2 $, $Date: 2011/10/11 18:46:54 $
 */
public class TotaisMsgBean {

    private String descricao;

    private Long totalMsgFunc;

    private Long totalMsgOrgao;

    private Long primeiroAcionamentoId;

    /*
     * Construtor
     */
    public TotaisMsgBean(String pDesc, Long ptotalMsgFunc, Long ptotalMsgOrgao,
            Long pAcionamento) {

        this.descricao = pDesc;
        this.totalMsgFunc = ptotalMsgFunc;
        this.totalMsgOrgao = ptotalMsgOrgao;
        this.primeiroAcionamentoId = pAcionamento;
    }

    /**
     * @return Retorna descricao.
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao
     *            descricao a ser atribuido.
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return Retorna totalMsgFunc.
     */
    public Long getTotalMsgFunc() {
        return totalMsgFunc;
    }

    /**
     * @param totalMsgFunc
     *            totalMsgFunc a ser atribuido.
     */
    public void setTotalMsgFunc(Long totalMsgFunc) {
        this.totalMsgFunc = totalMsgFunc;
    }

    /**
     * @return Retorna totalMsgOrgao.
     */
    public Long getTotalMsgOrgao() {
        return totalMsgOrgao;
    }

    /**
     * @param totalMsgOrgao
     *            totalMsgOrgao a ser atribuido.
     */
    public void setTotalMsgOrgao(Long totalMsgOrgao) {
        this.totalMsgOrgao = totalMsgOrgao;
    }

    /**
     * @return Retorna primeiroAcionamentoId.
     */
    public Long getPrimeiroAcionamentoId() {
        return primeiroAcionamentoId;
    }

    /**
     * @param primeiroAcionamentoId
     *            primeiroAcionamentoId a ser atribuido.
     */
    public void setPrimeiroAcionamentoId(Long primeiroAcionamentoId) {
        this.primeiroAcionamentoId = primeiroAcionamentoId;
    }
}