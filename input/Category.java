sdfg dsfg dsfg sdfg sdfg sdfg sdfgs
sdf gsdf gsdfg dsfg
dsf gsdfg sdfg 
 sdfg sdfgs

package br.gov.sample.demoiselle.auction5.bean;

import br.gov.component.demoiselle.common.pojo.extension.Description;
import br.gov.component.demoiselle.common.pojo.extension.EqualsField;
import br.gov.component.demoiselle.common.pojo.extension.IPojoExtension;
import br.gov.component.demoiselle.common.pojo.extension.OutputType;
import br.gov.framework.demoiselle.core.bean.IPojo;

/**
 * @author CETEC/CTJEE
 * @see IPojo
 */
@Entity
@Table(name = "categories")
@SequenceGenerator(name = "CategorySequence", sequenceName = "categories_seq")
public class Category implements IPojoExtension {

	private static final long serialVersionUID = 333L;

	@Id
	@GeneratedValue(generator = "CategorySequence", strategy = GenerationType.SEQUENCE)
	@EqualsField
	@Description
	private Short id;

	@Column(length = 30, nullable = false)
	@Description
	private String name;

	public Category() {
		super();
	}

	public Short getId() {
		return id;
	}

	public void setId(Short id) {
		this.id = id;
	}

}

