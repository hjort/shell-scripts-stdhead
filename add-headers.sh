#!/bin/bash

if [ $# -ne 2 ]
then
	echo "Uso: $0 <origem> <destino>"
	exit 1
fi

src="$1"
dest="$2"

if [ ! -d $src ]
then
	echo "Diretório de origem inexistente: $src"
	exit 2
fi

if [ -d $dest ]
then
	echo "Removendo diretório de destino: $dest"
	rm -rf $dest
fi

#FORMATOS="xml apt"
#FORMATOS="java properties xml jsp"
FORMATOS="java xml"

# TODO: properties, jsp => converter para ISO-8859 antes
# TODO: xml => incluir após tag <?xml...?> (se existente)

# copiar diretório de origem para destino
echo "Criando réplica da estrutura de origem..."
cp -R $src $dest

echo "Buscando lista de arquivos..."
for fmt in $FORMATOS
do
	# criar cabeçalhos para cada um dos formatos
	if [ -f hdr-$fmt.sh ]
	then
		./hdr-$fmt.sh > $fmt.header
	fi

	# buscar arquivos
	find $dest -type f -name "*.$fmt" > $fmt.list
done

temp="/tmp/$$"

for fmt in $FORMATOS
do
	echo "Incluindo cabeçalhos nos arquivos .$fmt..."
	while read arq
	do
		echo "$arq"

		# limpar cabeçalho pré-existente
		if [ -f clr-$fmt.sh ]
		then
			./clr-$fmt.sh $arq > $temp
			mv $temp $arq
		fi

		# incluir novo cabeçalho
		if [ -f $fmt.header ]
		then
			cat $fmt.header $arq > $temp
			mv $temp $arq
		fi

	done < $fmt.list
done

echo "Removendo arquivos temporários..."
rm -f *.header *.list
